﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.WinForms;
using System.Diagnostics;

namespace CefSharpTutorial
{
    public class CefCustomClass
    {

        private ChromiumWebBrowser browser;
        private Form1 form;

        public CefCustomClass(ChromiumWebBrowser browser, Form1 form)
        {
            this.browser = browser;
            this.form = form;
        }
        public void showDevTools()
        {
            browser.ShowDevTools();
        }
        public void openCmd()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd.exe","/K");
            Process.Start(processStartInfo);
        }
    }
}
