﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;

namespace CefSharpTutorial
{
    public partial class Form1 : Form
    {
        public ChromiumWebBrowser chromiumWebBrowser;
        public ChromiumWebBrowser browser;

        public Form1()
        {
            InitializeComponent();
            //InitializeChromium2();
            InitBrowser();
            //browser.ShowDevTools();
           browser.RegisterJsObject("cefCustomClass", new CefCustomClass(browser,this));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        //doesnt' work, with path relative or absolute, with url it works
        public void InitializeChromium()
        {
            /// nevyhnutne volat pred prvym registerJsObject
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            CefSettings cefSettings = new CefSettings();
            //cefSettings.BrowserSubprocessPath = @"x86\CefSharp.BrowserSubprocess.exe";
            //Cef.Initialize(cefSettings, performDependencyCheck: false, browserProcessHandler: null);
            Cef.Initialize(cefSettings);


            string startUrl = "http://ourcodeworld.com";


            // Now instead use http://ourcodeworld.com as URL we'll use the "page" variable to load our local resource
            string path = Application.StartupPath + @"\Html\index.html";
            path = @"A:\Documents\Programovanie+programy\cvic programy in C#\CefSharpTutorial\bin\Release\Html\index.html";

            MessageBox.Show("The file index.html The path is: " + path);
            if (!File.Exists(path))
            {
                MessageBox.Show("The file index.html doesn't exist! The path is: "+ path);
            }
            startUrl = path;
            chromiumWebBrowser = new ChromiumWebBrowser(startUrl);

            //add browser to the form
            this.Controls.Add(chromiumWebBrowser);
            chromiumWebBrowser.Dock = DockStyle.Fill;

            // Allow the use of local resources in the browser
            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            
            chromiumWebBrowser.BrowserSettings = browserSettings;


        }
        public void InitializeChromium2()
        {
            /// nevyhnutne volat pred prvym registerJsObject
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;

            CefSettings cefSettings = new CefSettings();
            Cef.Initialize(cefSettings);




            // Now instead use http://ourcodeworld.com as URL we'll use the "page" variable to load our local resource
            String path = Application.StartupPath + @"\Html\index.html";

            //path = "file:///Html/index.html";

            MessageBox.Show("The file index.html The path is: " + path);
            if (!File.Exists(path))
            {
                MessageBox.Show("The file index.html doesn't exist! The path is: " + path);
            }
            
            chromiumWebBrowser = new ChromiumWebBrowser(path);

            // Allow the use of local resources in the browser
            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            chromiumWebBrowser.BrowserSettings = browserSettings;

            //add browser to the form
            this.Controls.Add(chromiumWebBrowser);
           // chromiumWebBrowser.Dock = DockStyle.Fill;


        }

        public void InitBrowser()
        {

            /// nevyhnutne volat pred prvym registerJsObject
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;

            
            Cef.Initialize(new CefSettings());
            browser = new ChromiumWebBrowser("file:///Html/index.html"); // CefSharp needs a initial page...

            browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
            browser.MinimumSize = new System.Drawing.Size(20, 20);
            browser.Name = "webBrowser1";
            browser.TabIndex = 1;
            browser.Dock = DockStyle.Fill;

            
            this.Controls.Add(browser);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }
    }
}
